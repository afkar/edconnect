<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Test Website</title>

	<!-- Custom fonts for this template -->
	<link href="template/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link
		href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="template/css/sb-admin-2.min.css" rel="stylesheet" type="text/css">

	<!-- Custom styles for this page -->
	<link href="template/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

			<!-- Nav Item - Tables -->
			<li class="nav-item active">
				<a class="nav-link" href="tables.html">
					<i class="fas fa-fw fa-table"></i>
					<span>List Invoice</span></a>
			</li>

			<!-- Divider -->
			<hr class="sidebar-divider d-none d-md-block">

			<!-- Sidebar Toggler (Sidebar) -->
			<!-- <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div> -->

		</ul>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Begin Page Content -->
				<div class="container-fluid">

					<!-- DataTales Example -->
					<div class="card shadow mb-4">
						<div class="card-header py-3">
							<a href="#" class="btn btn-success btn-icon-split" style="float: right;" data-toggle="modal"
								data-target="#addInvoiceModal">
								<span class="icon text-white-50">
									<i class="fas fa-plus"></i>
								</span>
								<span class="text">Add Invoice</span>
							</a>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
									<thead>
										<tr>
											<th>Invoice ID</th>
											<th>Date</th>
											<th>Total Amount</th>
										</tr>
									</thead>
									<tbody>
                                    <?php foreach($dataInvoice as $invoice): ?>
                                        <tr>
                                            <td><?=$invoice->invoice_id?></td>
                                            <td class="date"><?=$invoice->date?></td>
                                            <td class="total"><?=$invoice->total_amount?></td>
                                        </tr>
                                    <?php endforeach ?>
									</tbody>
								</table>
								<div id="totalAmount">Total Amount = 0</div>
							</div>
						</div>
					</div>

				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- End of Main Content -->

			<!-- Footer -->
			<footer class="sticky-footer bg-white">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright &copy; Your Website 2020</span>
					</div>
				</div>
			</footer>
			<!-- End of Footer -->

		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>

	<!-- Logout Modal-->
	<div class="modal fade" id="addInvoiceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add Invoice</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<form id="formDetailInvoice">
						<div class="form-group row">
                            <div class="col-sm-3"><label for="exampleInputInvoiceID">Invoice ID</label></div>
							<div class="col-sm-6"><input type="text" class="form-control" id="invoice_id" required></div>
                            <div class="col-sm-3"><button type="button" class="btn btn-primary" onclick="getDetailInvoice(document.getElementById('invoice_id').value)">Load</button></div>
						</div>
                        <div class="form-group row">
                            <div class="col-sm-3"><label for="Date of Birth">Date</label></div>
                            <div class="col-sm-9"><input type="date" class="form-control" name="date" id="invoice_date"></div>
                        </div>

                        <div class="form-group row" style="margin-top: 5%;">
                            <div class="col-sm-3"><label for="Date of Birth">Product</label></div>
                            <select class="form-control col-sm-6" aria-label="Default select example" style="margin-left: 2%;" id="selectedProduct">
                                <option selected>Select Product</option>
                                <?php foreach($dataProduct as $product): ?>
                                    <option value="<?=$product->product_name?>"><?=$product->product_name?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3"><label for="Date of Birth">Amount</label></div>
                            <div class="col-sm-6"><input type="number" class="form-control" name="amount" id="productAmount"></div>
                            <div class="col-sm-3"><button type="button" class="btn btn-success" onclick="add_item(document.getElementById('selectedProduct').value,document.getElementById('productAmount').value)">Add</button></div>
                        </div>
                        <table class="table" id="tableDetailInvoice">
                            <thead>
                                <tr>
                                    <th hidden id="detail_invoice_id">Invoice Id</th>
                                    <th scope="col" id="product_name">Product</th>
                                    <th scope="col" id="product_amount">Amount</th>
                                    <th scope="col" id="action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
					</form>
				</div>
                <div id="totalDetailAmount" style="margin-left: 4%; font-weight: bold;">Total Amount = 0</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal" onclick="deleteInvoice(document.getElementById('invoice_id').value)">Delete</button>
                    <button class="btn btn-primary" type="button" onclick="saveInvoice(document.getElementById('invoice_id').value,document.getElementById('invoice_date').value)">Save</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Bootstrap core JavaScript-->
	<script src="template/vendor/jquery/jquery.min.js"></script>
	<script src="template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="template/vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="template/js/sb-admin-2.min.js"></script>

	<!-- Page level plugins -->
	<script src="template/vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="template/vendor/datatables/dataTables.bootstrap4.min.js"></script>


	 <!-- Page level custom scripts -->
	<script src = "template/js/demo/datatables-demo.js" ></script>

	<!-- Custom Function -->
	<script type="text/javascript">
        var baseURL= "<?php echo base_url();?>";
        var delete_id = [];
		$(document).ready(function () {
			newSum(true);
			$("input[type=search]").each(function () {
				var that = this;
				$(this).keyup(function () {
					newSum.call(that);
				});
			});

			function newSum(isAllValue = false) {
				var sum = 0;
				$('.total').each(function () {
					sum += parseFloat(this.innerHTML); 
				});

				var date = $('.date')[0].innerHTML;

				if (isAllValue == true) {
					$('#totalAmount').text("Total All Amount = " + sum);
				} else {
					$('#totalAmount').text("Total Amount " + date + " = " + sum);
				}
			}

		});

        function detailSum(isAllValue = false) {
				var sum = 0;
				$('.detail_amount').each(function () {
					console.log("DETAIL ", this.innerHTML);
					sum += parseFloat(this.innerHTML); // or parseInt(this.value,10) if appropriate
				});
				
                $('#totalDetailAmount').text("Total Amount = " + sum);
		}

        function getDetailInvoice(id_invoice){
            delete_id = [];
            $("#tableDetailInvoice > tbody > tr").remove();
            $.ajax({
                url: baseURL + 'index.php/Welcome/getInvoiceDetail',
                method: 'post',
                data: {invoice_id: id_invoice},
                dataType: 'json',
                success: function(response){
                var len = response.length;
                if(len > 0){
                    // Success
                    $('#invoice_date').val(response[0].date);
                    for (var i = 0; i < len; i += 1) {
                        var inv_id = "item_" + i;
                        var del_item = "delete_item('" + inv_id + "'," + response[i].detail_invoice_id + ")";
                        $("#tableDetailInvoice").append("<tr id='"+inv_id+"'><td hidden>" + response[i].detail_invoice_id + "</td><td>" + response[i].product_name + "</td><td class='detail_amount'>" + response[i].amount + "</td><td><button type='button' class='btn btn-danger' onclick="+del_item+">Delete</button></td></tr>");
                    }
            
                }

                detailSum();
            
                }
            });
        }

        function deleteInvoice(id_invoice){
            $.ajax({
                url: baseURL + 'index.php/Welcome/deleteInvoice',
                method: 'post',
                data: {invoice_id: id_invoice},
                dataType: 'json',
                success: function(response){
                var len = response.length;
                if(len > 0){
                    // Success
                }

                window.location.href = baseURL;
            
                }
            });
        }

        function delete_item(id, detail_id = null){
            if(detail_id != null){
                var rowValues = {};
                rowValues["detail_invoice_id"] = detail_id;
                delete_id.push(rowValues);
            }
            $('#' +id).remove();
            detailSum();
        }

        function add_item(product_name, product_amount){
            var index = $("#tableDetailInvoice > tbody > tr").length + 1;
            var inv_id = "item_" + index;
            var del_item = "delete_item('" + inv_id + "')";
            $("#tableDetailInvoice").append("<tr id='"+inv_id+"'><td hidden></td><td>" + product_name + "</td><td class='detail_amount'>" + product_amount + "</td><td><button type='button' class='btn btn-danger' onclick="+del_item+">Delete</button></td></tr>");
            detailSum();
        }

        function saveInvoice(id_invoice, date_invoice){
            var arr = [];
            var $table = $('#tableDetailInvoice');
            var columnNames = $table.find('thead tr th').map(function(e) {
                return $(this)[0].id;
            });

            $table.find('tbody tr').each(function(){
                var rowValues = {};
                $(this).find('td').each(function(i) {
                    rowValues[columnNames[i]] = $(this).text();
                });
                arr.push(rowValues);
            });
            $.ajax({
                url: baseURL + 'index.php/Welcome/saveInvoice',
                method: 'post',
                data: {invoice_id: id_invoice,invoice_date: date_invoice, detail_invoice: arr, delete_item: delete_id},
                dataType: 'json',
                success: function(response){
                var len = response.length;
                if(len > 0){
                    // Success
            
                }
                delete_id = [];
                window.location.href = baseURL;
                }
            });
        }

	</script>

</body>

</html>
