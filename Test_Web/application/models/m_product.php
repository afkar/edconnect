<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class m_product extends CI_Model {
    
        public function getListProduct()
        {
            $sql = "SELECT 
                * 
            FROM mr_product 
            WHERE is_deleted = 0;";

            return $this->db->query($sql)
                        ->result();
        }
    }
    
    /* End of file ModelName.php */
    
?>
