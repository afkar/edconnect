<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class m_invoice extends CI_Model {
    
        public function getListInvoice()
        {
            $sql = "SELECT 
                        i.invoice_id,
                        i.date,
                        (SELECT SUM(tr_id.amount) 
                            FROM tr_invoice_detail tr_id
                            WHERE tr_id.invoice_id = i.invoice_id AND tr_id.is_deleted = 0
                        ) total_amount
                    FROM
                    mr_invoice i
                    WHERE i.is_deleted = 0;";

            return $this->db->query($sql)
                        ->result();
        }

        public function getInvoiceDetail($postData=array(), $invoice_id = null)
        {
            $response = array();
            $invoice_id = isset($invoice_id) ? $invoice_id : $postData['invoice_id'];
            $sql = "SELECT 
                id.invoice_id,
                i.date,
                id.detail_invoice_id,
                p.product_name,
                id.amount
            FROM tr_invoice_detail id
            INNER JOIN mr_product p ON id.product_id = p.product_id
            INNER JOIN mr_invoice i ON id.invoice_id = i.invoice_id
            WHERE id.is_deleted = 0 AND p.is_deleted = 0 AND i.is_deleted = 0 AND id.invoice_id = $invoice_id";

            $response = $this->db->query($sql)
            ->result_array();
            return $response;
        }

        public function deleteInvoice($postData=array())
        {
            $response = array();
            $invoice_id = $postData['invoice_id'];
            $this->db->delete('mr_invoice', array('invoice_id' => $invoice_id)); 
            $this->db->delete('tr_invoice_detail', array('invoice_id' => $invoice_id)); 
            $response = array("Success");
            return $response;
        }

        public function deleteInvoiceDetail($postData=array())
        {
            $response = array();
            $ids=array();
            foreach($postData as $key =>$value ){
                $ids[]=$value['detail_invoice_id'];
            }

            $this->db->where_in('detail_invoice_id',$ids);
            $this->db->delete('tr_invoice_detail');
            $response = array("Success");
            return $response;
        }

        public function saveInvoice($postData=array())
        {
            $response = array();
            $invoice_id = $postData['invoice_id'];
            $invoice_date = $postData['invoice_date'];
            $detail_invoice = $postData['detail_invoice'];
            // $delete_id = $postData['delete_id'];

            if(isset($postData['delete_item']) && COUNT($postData['delete_item']) > 0){
                $this->deleteInvoiceDetail($postData['delete_item']);
            }

            $data_invoice=$this->db->query("SELECT * FROM mr_invoice WHERE invoice_id = $invoice_id")->result();

            $object=array(
                'invoice_id' => $invoice_id,
                'date' => $invoice_date,
                'is_deleted' => 0
            );
            if(COUNT($data_invoice) > 0){
                // Update
                $this->db
                ->where('invoice_id',$invoice_id)
                ->update('mr_invoice', $object);
            } else {
                // Insert
                $this->db->insert('mr_invoice', $object);
                $data = $this->db->insert_id();
            }
            for($i = 0; $i < COUNT($detail_invoice); $i++){
                $detail_invoice_id = $detail_invoice[$i]['detail_invoice_id'];
                if($detail_invoice_id == ""){
                $productName = $detail_invoice[$i]['product_name'];
                $productAmount = $detail_invoice[$i]['product_amount'];
                $product_id = $this->db->query("SELECT * FROM mr_product WHERE product_name = '$productName'")->result()[0]->product_id;
                $object=array(
                    'invoice_id' => $invoice_id,
                    'product_id' => $product_id,
                    'amount' => $productAmount,
                    'is_deleted' => 0
                );
                $this->db->insert('tr_invoice_detail', $object);
                $data = $this->db->insert_id();
               }
            }
            $response = array("Success");
            return $response;
        }
        
    }
    
    /* End of file ModelName.php */
    
?>
