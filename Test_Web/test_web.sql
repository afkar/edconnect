-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 24, 2022 at 03:31 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `mr_invoice`
--

CREATE TABLE `mr_invoice` (
  `invoice_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mr_invoice`
--

INSERT INTO `mr_invoice` (`invoice_id`, `date`, `is_deleted`) VALUES
(1, '2022-06-21', 0),
(2, '2022-06-22', 0),
(3, '2022-06-16', 0),
(4, '2022-06-21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mr_product`
--

CREATE TABLE `mr_product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(1000) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mr_product`
--

INSERT INTO `mr_product` (`product_id`, `product_name`, `is_deleted`) VALUES
(1, 'Product 1', 0),
(2, 'Product 2', 0),
(3, 'Product 3', 0),
(4, 'Product 4', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_invoice_detail`
--

CREATE TABLE `tr_invoice_detail` (
  `detail_invoice_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount` int(100) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_invoice_detail`
--

INSERT INTO `tr_invoice_detail` (`detail_invoice_id`, `invoice_id`, `product_id`, `amount`, `is_deleted`) VALUES
(1, 1, 1, 1000, 0),
(2, 1, 2, 2000, 0),
(3, 2, 1, 3000, 0),
(4, 3, 3, 500, 0),
(9, 4, 3, 500, 0),
(12, 4, 2, 10000, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mr_invoice`
--
ALTER TABLE `mr_invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `mr_product`
--
ALTER TABLE `mr_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tr_invoice_detail`
--
ALTER TABLE `tr_invoice_detail`
  ADD PRIMARY KEY (`detail_invoice_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mr_product`
--
ALTER TABLE `mr_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tr_invoice_detail`
--
ALTER TABLE `tr_invoice_detail`
  MODIFY `detail_invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
