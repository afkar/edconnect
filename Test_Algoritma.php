<?php
    // Soal No 1
    $x = [-4, 3, -9, 0, 4, 1, 0];
    $positive = 0;
    $negative = 0;
    $zero = 0;
    foreach ($x as $key){
        if ($key > 0)
            $positive++;
        else if ($key < 0)
            $negative++;
        else
            $zero++;
    }
    echo "Soal No 1";
    echo "<br />";
    echo "Negative = " . $negative . ", Positive = " . $positive . ", Zero = " . $zero;

    echo "<br /><br /><br />";

    // Soal No 2

    echo "Soal No 2";
    $y = [1,2,3,4,5];
    $n = 5;
    $tot = 0;
    for($i = 0; $i <= COUNT($y); $i++){
        echo "<br />";
        echo $i . " - ";
        $tot = 0;
        for($z = 0; $z < $n; $z++){
            if($y[$z] > $i){
                echo " " . $y[$z];
                $tot++;
            } 
            if($tot < $n && $y[$z] == $y[COUNT($y) - 1]) {
                for($b = 0; $b < $n; $b++){
                    if($tot < $n){
                        echo " " . $y[$b];
                        $tot++;
                    }   
                }
            }
        }
    }

    echo "<br /><br /><br />";

    // Soal No 3

    echo "Soal No 3";
    echo "<br />";
    function dateFormat($date,$format){
        echo "Date : " . $date;
        echo "<br />";
        echo "Date Format : " . $format;
        $explode_date = explode("-",$date);
        $explode_format = explode("-",$format);
        $year = array_search("yyyy",$explode_format,true);
        $month = array_search("mm",$explode_format,true);
        $day = array_search("dd",$explode_format,true);
        // Jawaban Versi 1
        echo "<br /> <br />" . "Jawaban Versi 1 " . "<br />";
        echo "Tahun: " . $explode_date[$year] . " , Bulan: " . $explode_date[$month] ." , Tanggal: " . $explode_date[$day];
        // Jawaban Versi 2
        echo "<br /> <br />"  . "Jawaban Versi 2 " . "<br />";
        $year = strtoupper(substr($explode_format[$year], 0, 1));
        $month = substr($explode_format[$month], 0, 1);
        $day = substr($explode_format[$day], 0, 1);
        $i = 0;
        $convert_format = "";
        foreach($explode_format as $key){
            if($i == 0 || $i == 1){
                if($key == "yyyy"){
                    $convert_format .= $year . "-";
                    $i++;
                } else if($key == "mm") {
                    $convert_format .= $month . "-";
                    $i++;
                } else {
                    $convert_format .= $day . "-";
                    $i++;
                }
            } else {
                if($key == "yyyy"){
                    $convert_format .= $year;
                    $i++;
                } else if($key == "mm") {
                    $convert_format .= $month;
                    $i++;
                } else {
                    $convert_format .= $day;
                    $i++;
                }
            }
        }   
        $date = strtotime($date);
        $default_date = date($convert_format,$date);
        print_r($default_date);
    }

    dateFormat("2015-01-08","yyyy-dd-mm");



?>